# Run main function from app.py


from run_cam_surveillance import acquire_frame, detect_action
from utils.helpers_inout import load_config
import time
import os
import numpy as np
#import matplotlib.pyplot as plt
import cv2 as cv

from utils.helpers_mail import read_token_yaml
from utils.helpers_inout import read_cam_ip_yaml
from run_cam_surveillance import load_model, set_path_save_images
from run_cam_surveillance import acquire_frame, detect_action
from utils.helpers_inout import load_config

from utils.helpers_tf import (detection_object_list_tf_hub,
                                model_load_tf_hub)
from utils.helpers_acquisition import (capture_image_prep, init_video,
                                       read_labels, stop_video)
from utils.helpers_mail import send_mail_with_image, read_token_yaml, print_classes_found
from utils.helpers_motion import compute_motion_detection, compute_1d_motion_detection
from utils.helpers_inout import read_cam_ip_yaml
from run_cam_surveillance import load_model, run_detection, set_path_save_images

# -----------------------------------------------------------
config_file = "config/config.yaml"
line_sep_str = """ *** """

# Current time
time_start_app = time.time()
# -----------------------------------------------------------
# Load config file 
param = load_config(config_file)
model_format = param['detection']['model']['model_format']

# Camera ID
vid_id = param['acquisition']['cam_id']

# Acquisition parameters
time_delay = 60*param['global']['time_start'] # Delay in seconds
mode_ref_mean = param['acquisition']['mode_ref_mean']
num_frames_mean = param['acquisition']['num_frames_mean']

# Motion detection parameters
num_frames_interval = param['motion']['num_frames_interval']
motion_thresh_val = param['motion']['thresh_val']
motion_thresh_percent = param['motion']['thresh_percent']

# Classes to detect: Raise detection if class found with any probability
classes_searched = param['detection']['classes_searched']
score_thres = param['detection']['score_thres']
num_frames_freq = param['detection']['num_frames_freq']

# Path save images
path_save = set_path_save_images(param)
# -----------------------------------------------------------
# Config send mail 
if param['mail']['mode_mail'] is True:
    # Read token from yaml file
    mt_config = read_token_yaml(param['mail']['path_config'])
else:
    mt_config = None

# -----------------------------------------------------------
# Config ip-cam 
if param['acquisition']['mode_video'] == 'Camera-IP':
    vid_id = read_cam_ip_yaml(param['acquisition']['path_config'])
# -----------------------------------------------------------
# Downsample
downsample = param['acquisition']['downsample']    
if downsample is True:
    downsample_res = param['acquisition']['downsample_res']
else:
    downsample_res = None
# ---------------------------------------E--------------------
# Model format
print("Loading the model ...")
detector, labels, dtype, output_keys = load_model(param)
print("Model loaded")

mode_display = True
# -----------------------------------------------------------
# Call AI surveillance
def call_ai_surv():
    # Init camera acquire
    cam, vid_dim, fps = init_video(vid_id=vid_id)
    (width, height, num_frames) = vid_dim
    print("Init video")
    if downsample is True:
        width = downsample_res[0]
        height = downsample_res[1]

    # Capture initial frames
    for i in range(20): 
        # Acquire frames
        frame = capture_image_prep(cam, downsample_res=downsample_res)

    # Acquire reference frame
    frame_ref = capture_image_prep(cam, mode_mean=mode_ref_mean, 
                                   num_frames_mean=num_frames_mean, 
                                   downsample_res=downsample_res)    
    # Return variables to app
    frame_det = None
    txt_detected_upd = "no detection"        # Acquire frames
    time_now = time.strftime("%H:%M:%S")       
 
    # Init motion detection
    motion_detected = False
    # Init interval of frames         
    #frames = np.zeros((num_frames_interval, height, width, 3), dtype=np.uint8)
    frames = []
    count_frames_interval = 0 
    count_frames = 0
    while (cam.isOpened()):    
        # -----------------------------------
        frame = capture_image_prep(cam, downsample_res=downsample_res)
        # -----------------------------------
        # Create an interval of frames
        if count_frames_interval < num_frames_interval:
            #frames[count_frames_interval] = frame
            frames.append(frame)
            count_frames_interval += 1
        else:
            # -----------------------------------
            # Motion detection based on max and min projection
            #("Computing motion detection")
            frames = np.array(frames)
            t_ref = time.time()
            if (time.time() - time_start_app > time_delay):
                motion_detected = compute_motion_detection(frames, 
                                                    thresh = motion_thresh_val, 
                                                    thresh_percent = motion_thresh_percent)
            #print(f"Time motion detection: {time.time() - t_ref}")
            # -----------------------------------         
            # Object detection
            if (motion_detected):  
                # On each frame
                t_ref = time.time()
                for i, frame in enumerate(frames):
                    # Detection and action (send mail) on each frame
                    if (i % num_frames_freq == 0):
                        txt_detected_upd, frame_det_upd = detect_action(detector, frame, count_frames, classes_searched, 
                                        model_format=model_format, score_thres=score_thres,
                                        path_save=path_save, output_keys=output_keys, dtype=dtype, labels=labels)
                        # Save frame with detection if any
                        if frame_det_upd is not None:
                            frame_det = frame_det_upd
                            # Current time in format HH-MM-SS
                            time_now = time.strftime("%H:%M:%S")          
                            
                print(f"Time detection: {time.time() - t_ref}")
            else:
                # Current time in format HH-MM-SS
                time_now = time.strftime("%H:%M:%S")      
                txt_detected_upd = "no detection"  

            # -----------------------------------
            # Reset interval
            #frames = np.zeros((num_frames_interval, height, width, 3), dtype=np.uint8)
            frames = []
            count_frames_interval = 0 

        # Update text
        txt_detected = f'{txt_detected_upd} at {time_now}'
        count_frames += 1 

        if mode_display is True:
            # Plot frame
            if frame is not None:  
                cv.imshow('Stream', frame)
                cv.waitKey(1)

            # Plot frame_det
            if frame_det is not None:
                cv.imshow('Detection', frame_det)
                cv.waitKey(1)

            # Display the plot
            #plt.show()
    

call_ai_surv()
