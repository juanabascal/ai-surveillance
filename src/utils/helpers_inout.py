import os

import yaml
from yamlinclude import YamlIncludeConstructor


def load_config(config_file):

    YamlIncludeConstructor.add_to_loader_class(loader_class=yaml.FullLoader, 
                                           base_dir=os.path.split(config_file)[0])

    # Import YAML parameters from config/config.yaml
    with open(config_file, 'r') as stream:
        param = yaml.load(stream, yaml.FullLoader)
        print(yaml.dump(param))
    return param

def read_cam_ip_yaml(path_yaml='../../cam_ip/foscam.yaml'):
    """Read token from yaml file        
    """
    with open(path_yaml, 'r') as stream:
        try:
            mt_config = yaml.safe_load(stream)                   
        except yaml.YAMLError as exc:
            print(exc)
    username = mt_config['username']
    passw = mt_config['token']
    port = mt_config['port'][0]  
    ip = mt_config['ip']  
    host = mt_config['host'] 
    vid = f"http://{username}:{passw}@{ip}:{port}/{host}"
    return vid
