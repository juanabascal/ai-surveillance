import numpy as np
import matplotlib.pyplot as plt

def compute_max_min_interval(frames):
    # Compute max and min of the interval
    max_interval = np.max(frames, axis=0)
    min_interval = np.min(frames, axis=0)
    return max_interval, min_interval

def compute_img_thresh(img_ref, img, thresh = 50):
    # Compute min_max_thresh
    img_diff = (np.abs(img_ref-img)).astype(np.uint8)
    img_diff_thresh = img_diff > thresh
    return img_diff_thresh

def compute_num_pix_thresh(img, thresh_percent = 0.1):
    # Number of pixels above threshold
    img_shape = img.shape
    width, height = img_shape[0], img_shape[1]
    num_pix_thresh = np.sum(img)          
    # If number of pixels above threshold is above 10% of the image
    if num_pix_thresh > thresh_percent*(width*height):
        motion_detected = True
    else:
        motion_detected = False
    return motion_detected

def compute_motion_detection(frames, thresh = 50, thresh_percent = 0.1):
    # Init motion detection
    frame_ref = frames[0]

    # Compute max and min of the interval
    max_interval, min_interval = compute_max_min_interval(frames)

    # Compute threshold on min
    min_thresh = compute_img_thresh(frame_ref, min_interval, thresh = thresh)
    # Number of pixels above threshold
    min_motion_detected = compute_num_pix_thresh(min_thresh, thresh_percent = thresh_percent)
    
    # Compute threshold on max
    #max_thresh = compute_img_thresh(frame_ref, max_interval, thresh = thresh)
    # Number of pixels above threshold
    #max_motion_detected = compute_num_pix_thresh(max_thresh, thresh_percent = thresh_percent)

    # Motion detected if min or max
    #motion_detected = min_motion_detected or max_motion_detected
    motion_detected = min_motion_detected
    
    if False:
        import PIL.Image as Image
        im = Image.fromarray(img_ref)
        im.show()
        im = Image.fromarray(img)
        im.show()

        im = Image.fromarray(img_diff)
        im.show()

        im = Image.fromarray((255*min_max_thresh).astype(np.uint8))
        im.show()  
    return motion_detected

def compute_1d_motion_detection(frames, thresh = 1, display=False):
    frames_bw = np.mean(frames, axis=3).astype(np.uint8)
    frames_bw_mean_spatial = np.mean(frames_bw, axis=(1,2)).astype(np.uint8)
    if display:
        fig = plt.figure()
        plt.plot(frames_bw_mean_spatial)
        fig.savefig('tmp.png')

        fig = plt.figure()
        plt.imshow(frames[0].astype(np.uint8))
        fig.savefig('frame.png')

    # Threshold on variance
    motion_detected = np.var(frames_bw_mean_spatial) > thresh
    return motion_detected

