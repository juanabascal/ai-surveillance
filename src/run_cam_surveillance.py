# AI surveillance 
#
# Detect objects in a video stream from a camera and save images when an object 
# from a list of searched objects is detected. 
#
# Parameters sets on config_hub

import os
from datetime import datetime as dt
from datetime import timedelta

#import pandas as pd
import cv2 as cv
import numpy as np

from utils.helpers_acquisition import (capture_image_prep, init_video,
                                       read_labels, stop_video)
from utils.helpers_analysis import display_image as display_image_cv
from utils.helpers_inout import load_config
from utils.helpers_mail import send_mail_with_image, read_token_yaml, print_classes_found

# -----------------------------------------------------------
# Working directory
print(f"Working directory: {os.getcwd()}")
# -----------------------------------------------------------
# Import YAML parameters from config/config.yaml
config_file = "config/config.yaml"

# Load config file 
param = load_config(config_file)
model_format = param['detection']['model']['model_format']

# -----------------------------------------------------------
# Model format
if model_format == 'yolov8':
    from utils.helpers_yolov8 import detection_object_yolov8, model_load_yolov8
elif model_format == 'hub':
    from utils.helpers_tf import (detection_object_list_tf_hub,
                                model_load_tf_hub)
elif model_format == 'pb':
    from utils.helpers_tf import model_load_tf_SavedModel
elif model_format == 'yolov3_tf':
    from utils.helpers_tf_yolov3 import (WeightReader,
                                        detection_object_tf_yolov3,
                                        make_yolov3_model)
elif model_format == 'tflite':
    from utils.helpers_tflite import detection_object_tflite, model_load_tflite
# -----------------------------------------------------------
# Config send mail 
if param['mail']['mode_mail'] is True:
    # Read token from yaml file
    mt_config = read_token_yaml(param['mail']['path_config'])
else:
    mt_config = None
# -----------------------------------------------------------

def acquire_frame(param):
    """Acquire frame from camera for streaming"""
    # Camera ID
    vid_id = param['acquisition']['cam_id']

    # Acquisition parameters
    mode_ref_mean = param['acquisition']['mode_ref_mean']
    num_frames_mean = param['acquisition']['num_frames_mean']

    # Init camera acquire
    cam, vid_dim, fps = init_video(vid_id=vid_id)
    (width, height, num_frames) = vid_dim

    # Capture first frame
    frame = capture_image_prep(cam, mode_mean=mode_ref_mean,
                            num_frames_mean=num_frames_mean)   
    # Acquire frames
    count_frames = 0
    while(cam.isOpened()):    
        frame = capture_image_prep(cam)
        if frame is None:
            print("Frame not acquired!!!")

        count_frames += 1 
     
        return frame

def set_path_save_images(param):
    """Set path for saving images"""
    # Path save images
    now = dt.now().strftime("%Y%m%d-%H%M")
    path_save = os.path.join(param['checkpoints']['path_save_images'], now)
    if not os.path.exists(path_save):
        os.makedirs(path_save)
        print(f"Create folder for saving images {path_save}")
    else:
        print(f"Folder for saving images {path_save} already exists")
    path_save = os.path.join(path_save, f"{param['checkpoints']['name_save']}-{now}")
    return path_save

def load_model(param):
    """Load object detector"""

    # Detection parameters
    detection_task = param['detection']['task']
    path_model = param['detection']['model']['path_model']
    model_format = param['detection']['model']['model_format']
    dtype = param['detection']['model']['dtype']
    output_keys = param['detection']['model']['output_keys']
    signatures = param['detection']['model']['signatures']
    require_labels = param['detection']['model']['require_labels']
    path_labels = param['detection']['model']['path_labels']

    # -----------------------------------------------------------
    # Load object detector
    if model_format == 'yolov8':
        detector = model_load_yolov8(path_model, detection_task)
    elif model_format == 'hub':
        detector = model_load_tf_hub(path_model, signatures=signatures)
    elif model_format == 'pb':
        detector = model_load_tf_SavedModel(path_model)
    elif model_format == 'yolov3_tf':
        # define the yolo v3 model
        detector = make_yolov3_model()
        print(detector.summary())

        # load the weights
        weight_reader = WeightReader(path_model)

        # set the weights
        weight_reader.load_weights(detector)
    elif model_format == 'tflite':
        detector = model_load_tflite(path_model, model_format)

    # Load labels
    if require_labels is True:
       labels = read_labels(path_labels)
    else:
        labels = None
    return detector, labels, dtype, output_keys

def run_detection(detector, frame, count_frames, classes_searched, model_format='hub', score_thres=0.2,
                  path_save=None, output_keys=None, dtype=None, labels=None, mode_display=True):
    """Run object detection and save images if object detected

    result, classes_searched_positive = run_detection(detector, labels, frame, count_frames, classes_searched, score_thres,
                    path_save=None, model_format=None, output_keys=None, dtype=None)
    """
        
    # Object detection
    #time_start = dt.now()
    #if (c_i.MODE_DETECTION_OBJECT is True) and (count_frames % c_i.OBJECT_NUM_FRAMES == 0):   
    path_image_detected = None # For mail        
    if model_format == 'yolov8':
        result, classes_searched_positive = detection_object_yolov8(detector, frame, 
                                                                count_frames, classes_searched, 
                                                                labels, score_thres=score_thres, 
                                                                path_save=path_save)  
    elif  model_format == 'yolov3_tf': 
        result, classes_searched_positive = detection_object_tf_yolov3(detector, frame, 
                                                                count_frames, classes_searched, 
                                                                score_thres=score_thres, 
                                                                path_save=path_save)           
    elif model_format == 'hub'or model_format == 'pb':
        result, classes_searched_positive, path_image_detected, image_with_boxes = detection_object_list_tf_hub(detector, frame, 
                                                                count_frames, classes_searched, output_keys,
                                                                score_thres=score_thres, 
                                                                path_save=path_save, dtype=dtype, 
                                                                labels=labels, 
                                                                mode_display=mode_display)  
    elif model_format == 'tflite':
        result, classes_searched_positive = detection_object_tflite(detector, frame, 
                                                                count_frames, classes_searched)#, 
                                                                #score_thres=score_thres, 
                                                                #path_save=path_save, dtype=dtype, 
                                                                #labels=labels)  
    return result, classes_searched_positive, path_image_detected, image_with_boxes

def send_mail(param, mt_config, classes_searched_positive, time_sent_mail, num_sent_mail, path_image_detected):
    """Send mail with image"""

    # Send mail if not mail sent in the last minute and less than 10 mails sent
    time_send_interval = (dt.now() - time_sent_mail).total_seconds() > param['mail']['time_interval']*60
    if time_send_interval and (num_sent_mail < param['mail']['num_stop']):
        time_sent_mail = dt.now()
        print(f"Send mail with image at {time_sent_mail}")
    
        # Format detection results
        mail_text = print_classes_found(classes_searched_positive)
        send_mail_with_image(mt_config=mt_config, 
                        receiver=param['mail']['receiver'], 
                        subject=param['mail']['subject'], 
                        name=param['mail']['name'],
                        text=mail_text,
                        path_image=path_image_detected)
        num_sent_mail =+ 1
    return time_sent_mail, num_sent_mail


def detect_action(detector, frame, count_frames, classes_searched, model_format='hub', score_thres=0.2,
                  path_save=None, output_keys=None, dtype=None, labels=None, mode_display=True):
    result, classes_searched_positive, path_image_detected, image_with_boxes = run_detection(
                                    detector, frame, count_frames, 
                                    classes_searched, model_format=model_format, score_thres=score_thres,                                                                       
                                    path_save=path_save, output_keys=output_keys, 
                                    dtype=dtype, labels=labels, mode_display=False
                                    )    
    if classes_searched_positive:
        # Extract class and score
        txt_detected = [f"{class_ent[0]} ({class_ent[1]:.1f})" for class_ent in classes_searched_positive]
        txt_detected = ", ".join(txt_detected)
        
        frame_det = image_with_boxes
        # Send mail
        if param['mail']['mode_mail'] is True:
            send_mail_with_image(mt_config, 
                                param['mail']['receiver'], 
                                subject="ai-surv detection", 
                                name="ai-urv",
                                text="Detected object and probability: " + txt_detected,
                                path_image=path_image_detected)
        # Print classes found
        print_classes_found(classes_searched_positive)
    else:
        txt_detected = "no detection"
        frame_det = None
    return txt_detected, frame_det


################################################################
# Main function
def run_ai_surv(param, mt_config=None, yield_frame=False):
    # -----------------------------------------------------------
    # Camera ID
    vid_id = param['acquisition']['cam_id']

    # Acquisition parameters
    mode_ref_mean = param['acquisition']['mode_ref_mean']
    num_frames_mean = param['acquisition']['num_frames_mean']

    # Classes to detect: Raise detection if class found with any probability
    classes_searched = param['detection']['classes_searched']
    # -----------------------------------------------------------
    # Path save images
    path_save = set_path_save_images(param)
    # -----------------------------------------------------------
    # Model format
    detector, labels, dtype, output_keys = load_model(param)
    # -----------------------------------------------------------
    # Initialize time start
    time_start = dt.now()

    # Initialize time sent last mail and add five minutes    
    time_sent_mail = time_start - timedelta(minutes=param['mail']['time_start'])
    num_sent_mail = 0

    # Init camera acquire
    cam, vid_dim, fps = init_video(vid_id=vid_id)
    (width, height, num_frames) = vid_dim

    # Capture first frame
    frame = capture_image_prep(cam, mode_mean=mode_ref_mean,
                            num_frames_mean=num_frames_mean)
        
    if (param['display']['mode_display'] is True) and (yield_frame is False):
        cv.imshow('Capturing', frame)
        k = cv.waitKey(20)
    print(f"Init camera {vid_id} with frames {width}x{height}, fps={int(fps)}\n")
    # -----------------------------------------------------------
    # -----------------------------------------------------------
    # Acquire frames
    count_frames = 0
    while(cam.isOpened()):    
        frame = capture_image_prep(cam)
        if frame is None:
            print("Frame not acquired!!!")

        count_frames += 1 
        # -----------------------------------------------------------
        # -----------------------------------------------------------
        # OBJECT DETECTION
        #frame = cv.imread("images/photo.jpg")
        num_frames_freq = param['detection']['num_frames_freq']
        score_thres=param['detection']['score_thres']

        # Start detection after a certain number of minutes and detection frequency
        time_from_start = dt.now()-time_start
        if (count_frames % num_frames_freq == 0) and \
            (time_from_start.total_seconds() > param['global']['time_start']*60):
            # -----------------------------------------------------------
            # Display frame
            if (param['display']['mode_display'] is True) and (yield_frame is False):
                # CV_CAP_PROP_POS_MSEC Current position of the video file in milliseconds.
                # CV_CAP_PROP_POS_FRAMES
                # print(f"Read {count_frames} frames ({count_frames//(60*fps)} min)")     
                display_image_cv(frame, 'Capturing')
                k = cv.waitKey(20)
            # -----------------------------------------------------------
            # Object detectionE
            result, classes_searched_positive, path_image_detected, image_with_boxes = run_detection(
                                                detector, frame, count_frames, 
                                                classes_searched, model_format=model_format, score_thres=score_thres,                                                                       
                                                path_save=path_save, output_keys=output_keys, 
                                                dtype=dtype, labels=labels,
                                                )    
            if yield_frame is True:
                yield image_with_boxes
            # -----------------------------------------------------------
            # Take action if object detected
            if classes_searched_positive:
                print(f"Object detected: {classes_searched_positive}")
                # -----------------------------------------------------------
                # Send mail with image 
                if param['mail']['mode_mail'] is True:                    
                    # Send mail if not mail sent in the last minute and less than 10 mails sent
                    time_sent_mail, num_sent_mail = send_mail(param, mt_config, classes_searched_positive, 
                                                              time_sent_mail, num_sent_mail, path_image_detected)
            # -----------------------------------------------------------                
    stop_video(cam) 

if __name__ == "__main__":
    run_ai_surv(param, mt_config=mt_config, )