import gradio as gr
from run_cam_surveillance import acquire_frame
from utils.helpers_inout import load_config
import time
import numpy as np

config_file = "config/config.yaml"

# Load config file 
param = load_config(config_file)

# Define function to get image
def get_image(wait_time):
    time.sleep(wait_time * 60)
    while True:
        frame = acquire_frame(param)
        yield frame

# Define function to display placeholder image
def placeholder_image():
    return np.zeros((480, 640, 3), dtype=np.uint8)

# Define function to start surveillance
def start_surveillance(wait_time):
    iface_det = gr.Interface(fn=get_image, 
                             inputs=wait_time, 
                             outputs="image",
                             title="Surveillance App",
                             description="Surveillance output")
    iface_det.launch(share=True)

# Define Gradio interface
iface = gr.Interface(fn=placeholder_image, 
                     inputs=gr.inputs.Image(label="Click to start surveillance"), 
                     outputs="image",
                     title="Surveillance App",
                     description="Click the button to start surveillance:")

# Add start button to interface
iface.add_input("button", label="Start surveillance", 
                onclick=start_surveillance)

# Run the interface
iface.launch()